var v = 10;
var soma = 0;


if(v > 15){
	console.log("Erro: atividade 1 não pode ter mais que 15 pontos lançados!");
}else{
	soma += v;

	v = 12;

	if(v > 25){
		console.log("Erro: atividade 2 não pode ter mais que 25 pontos lançados!");
	}else{
		soma += v;

		v = 20;

		if(v > 25){
			console.log("Erro: atividade 3 não pode ter mais que 25 pontos lançados!");
		}else{
			soma += v;

			v = 20;

			if(v > 35){
				console.log("Erro: atividade 4 não pode ter mais que 35 pontos lançados!");
			}else{
				soma += v;
				
				if(soma >= 60){
					console.log("Aluno foi aprovado");
				}else{
					let resultado = 60 - soma;

					console.log("Aluno foi reprovado, faltando " + resultado + " pontos para a pontuação mínima de aprovação");
				}
			}
		}
	}
}
