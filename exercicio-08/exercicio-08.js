var martelo_a = 16;
var martelo_d = 2;

var florete_a = 10;
var florete_d = 7;

var arco_a = 20;
var arco_d = 0;

var varinha_a = 17;
var varinha_d = 1;

var ataque_1 = 0;
var ataque_2 = 0;

var defesa_1 = 0;
var defesa_2 = 0;

var nome1 = "Breno";
var arma1 = "Varinha de Inverno";
var forca1 = 70;
var vida1 = 1800;

var nome2 = "Júlia";
var arma2 = "Arco e Flechas";
var forca2 = 90;
var vida2 = 1800;

var valor_aleatorio = 20;

ataque_1 = (forca1 * valor_aleatorio) + varinha_a;
ataque_2 = (forca2 * valor_aleatorio) + arco_a;

defesa_1 = ataque_2 - (varinha_d*100);
defesa_2 = ataque_1 - (arco_d*100);

if(defesa_1 < 1800 && defesa_2 < 1800){
	console.log(nome1 + " perdeu " + defesa_1 + " pontos de vida no ataque de " + nome2);
	console.log(nome2 + " perdeu " + defesa_2 + " pontos de vida no ataque de " + nome1);
	console.log("Todos sobreviveram");
}else if(defesa_1 > 1800 && defesa_2 < 1800){
	console.log(nome1 + " perdeu " + defesa_1 + " pontos de vida no ataque de " + nome2);
	console.log(nome2 + " perdeu " + defesa_2 + " pontos de vida no ataque de " + nome1);
	console.log(nome1 + " morreu");
}else if(defesa_1 < 1800 && defesa_2 > 1800){
	console.log(nome1 + " perdeu " + defesa_1 + " pontos de vida no ataque de " + nome2);
	console.log(nome2 + " perdeu " + defesa_2 + " pontos de vida no ataque de " + nome1);
	console.log(nome2 + " morreu");
}else{
	console.log(nome1 + " perdeu " + defesa_1 + " pontos de vida no ataque de " + nome2);
	console.log(nome2 + " perdeu " + defesa_2 + " pontos de vida no ataque de " + nome1);
	console.log("Todos morreram");
}