let aluno1 = 38.5;
let aluno2 = 86.4;
let aluno3 = 60.2;

let media = (aluno1 + aluno2 + aluno3)/3;

console.log("O aluno 1 teve um aproveitamento de: ", aluno1);
console.log("O aluno 2 teve um aproveitamento de: ", aluno2);
console.log("O aluno 3 teve um aproveitamento de: ", aluno3);
console.log("A média da nota final de todos os alunos que frequentaram o presencial foi de: ", media);